# this is a custom new_note template

 * %{ISSUE_ID}: issue IID
 * %{ISSUE_PATH}: project path appended with the issue IID
 * %{NOTE_TEXT}: note text
 * %{UNSUBSCRIBE_URL}: unsubscribe URL
 * %{SYSTEM_HEADER}: system header message
 * %{SYSTEM_FOOTER}: system footer message
 * %{ADDITIONAL_TEXT}: custom additional text
