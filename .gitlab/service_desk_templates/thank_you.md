# this is a custom thank you template 

 * %{ISSUE_ID}: issue IID
 * %{ISSUE_PATH}: project path appended with the issue IID
 * %{UNSUBSCRIBE_URL}: unsubscribe URL
 * %{SYSTEM_HEADER}: system header message
 * %{SYSTEM_FOOTER}: system footer message
 * %{ADDITIONAL_TEXT}: custom additional text
